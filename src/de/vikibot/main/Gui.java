package de.vikibot.main;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.*;
class Gui{
    public static void main(String args[]){
    	Sound sound = new Sound();
       JFrame frame = new JFrame("My First GUI");
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setSize(300,300);
       JPanel panel = new JPanel();
       JButton buttonRecord = new JButton("Record");
       buttonRecord.addActionListener(new ActionListener() { 
    	   public void actionPerformed(ActionEvent e) { 
    	     System.out.println("record button pressed");
    	     Thread threadRecording = new Thread(new Runnable() {
    	            public void run() {
    	            	sound.startRecording();
    	            }
    	        });
    	     threadRecording.start();
    	   } 
    	 });
       JButton buttonPlay = new JButton("Play");
       buttonPlay.addActionListener(new ActionListener() { 
    	   public void actionPerformed(ActionEvent e) { 
    	     System.out.println("play button pressed");
    	     Thread threadPlaying = new Thread(new Runnable() {
        	        public void run() {
        	           	sound.startPlaying();
        	        }
        	 });
    	     threadPlaying.start();
    	   } 
    	 });
       JButton buttonStop = new JButton("Stop");
       buttonStop.addActionListener(new ActionListener() { 
    	   public void actionPerformed(ActionEvent e) { 
    	     System.out.println("stop button pressed");
    	     sound.finish();
    	     Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
    	     for(Thread t: threadSet) {
    	    	 //System.out.println(t.toString());
    	    	 if(t.getName().equals("Java Sound Event Dispatcher")) {
    	    		 t.interrupt();
    	    		 System.out.println(t.toString());
    	    	 }
    	     }
    	   } 
    	 });
       panel.add(buttonRecord);
       panel.add(buttonStop);
       panel.add(buttonPlay);
       frame.getContentPane().add(panel); // Adds Button to content pane of frame
       frame.setVisible(true);
    }
}
