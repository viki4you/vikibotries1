package de.vikibot.main;

import javax.sound.sampled.*;
import java.io.*;

public class Sound{
    // path of the wav file
    File wavFile = new File("RecordAudio.wav");
 
    Clip audioClip;
    
    // format of audio file
    AudioFileFormat.Type fileType = AudioFileFormat.Type.WAVE;
 
    // the line from which audio data is captured
    TargetDataLine line;
 
    /**
     * Defines an audio format
     */
    AudioFormat getAudioFormat() {
        float sampleRate = 16000;
        int sampleSizeInBits = 8;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = true;
        AudioFormat format = new AudioFormat(sampleRate, sampleSizeInBits,
                                             channels, signed, bigEndian);
        return format;
    }
    
    /**
     * Captures the sound and record into a WAV file
     */
    void startRecording() {
        try {        	
            AudioFormat format = getAudioFormat();
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
 
            // checks if system supports the data line
            if (!AudioSystem.isLineSupported(info)) {
                System.out.println("Line not supported");
                System.exit(0);
            }
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(format);
            line.start(); 
 
            AudioInputStream ais = new AudioInputStream(line);
 
            // start recording
            System.out.println("Start recording...");
            AudioSystem.write(ais, fileType, wavFile);
            System.out.println("Start recording... stopped.");
 
        } catch (LineUnavailableException ex) {
            ex.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
 
    /**
     * Closes the target data line to finish capturing and recording
     */
    void finish() {
    	if(line != null && line.isOpen()) {
    		line.stop();
    		line.close();
    		System.out.println("Recording Finished");
    	}
    	if(audioClip != null && audioClip.isOpen()) {
    		audioClip.stop();
    		audioClip.close();
    		System.out.println("Playing Finished");
    	}
    }
    
    void startPlaying() {
	    try {
    		AudioInputStream audioStream = AudioSystem.getAudioInputStream(wavFile);
	    	 
	        AudioFormat format = audioStream.getFormat();
	
	        DataLine.Info info = new DataLine.Info(Clip.class, format);
	
	        audioClip = (Clip) AudioSystem.getLine(info);
	
	        //audioClip.addLineListener(this);
	
	        audioClip.open(audioStream);
	         
	        audioClip.start();
	        System.out.println("Start playing...");
	    } catch (UnsupportedAudioFileException ex) {
	        System.out.println("The specified audio file is not supported.");
	        ex.printStackTrace();
	    } catch (LineUnavailableException ex) {
	        System.out.println("Audio line for playing back is unavailable.");
	        ex.printStackTrace();
	    } catch (IOException ex) {
	        System.out.println("Error playing the audio file.");
	        ex.printStackTrace();
	    }    	
    }
}
